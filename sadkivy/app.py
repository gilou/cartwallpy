from kivy.app import App
from kivy.core.audio import SoundLoader
from kivy.logger import Logger
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.properties import ObjectProperty
from glob import glob
from os.path import basename, join

class Cart(Button):
    """Single cart to hold an audio file"""

    sound = ObjectProperty(None)

    def on_touch_down(self, touch):
        # Kivy gives us touch events outside or position, so just let the tree handle it…
        if not self.collide_point(*touch.pos):
            return False

        if self.sound and 'button' in touch.profile and touch.button == 'right' and self.sound.state == 'play':
            self.sound.stop()
            self.fake_release()
            return super().on_touch_down(touch)

        if not self.sound:
            self.text = 'Cart Touched'
        else:
            if self.sound.state == 'play':
                self.sound.stop()
                self.sound.play()
                self.background_normal = self.background_down
            else:
                self.sound.play()
                # Keep the background as if still clicking
                self.background_normal = self.background_down
        return super().on_touch_down(touch)

    def fake_release(self):
        self.background_normal = self._button_background
    
    def on_sound_stop(self, obj):
        """Callback for sound stopping, restoring the background to the normal one"""
        if obj.state == 'stop':
            self.fake_release()
    
    def __init__(self, filename=None, **kwargs):
        super().__init__(**kwargs)
        self.text = 'Cart'
        # Store the default 'normal' background
        self._button_background = self.background_normal
        
        # If we're called with a filename, let's load it as a sound carelessly
        if filename:
            self.sound = SoundLoader.load(filename)
            # Make sure we get a callback when sound stops
            self.sound.bind(on_stop=self.on_sound_stop)
            self.text = basename(self.sound.source)
            Logger.info('Cart: Loaded {}'.format(self.sound.source))

class Cartwall(GridLayout):
    """The wall of carts"""
    def __init__(self, media_dirs=None, **kwargs):
        super().__init__()
        self.cols = 4
        if media_dirs:
            self._media_dirs = media_dirs
            for dir in media_dirs:
                Logger.info('Cart: Loading files from {}'.format(dir))
                files = []
                for ext in ('mp3', 'opus'):
                    files += glob(join(dir, '*.' + ext))
                for filename in sorted(files):
                    self.add_widget(Cart(filename))

class Cartwallpy(App):
    """Main app"""
    def __init__(self, media_dirs=None):
        super().__init__()
        self._media_dirs = media_dirs
        self.title = "{} {}".format(self.name, " ".join(map(basename, media_dirs)))
        Logger.info('Base: Initialized app with {}'.format(media_dirs))
        
    
    def build(self):
        return Cartwall(self._media_dirs)