import argparse
import sys


parser = argparse.ArgumentParser(description='Start cartwallpy, a simple cartwall player')
parser.add_argument('-m', '--media-dir', default='.', nargs='*', required=False, help='base directory for the files')

if __name__ == '__main__':
    """
    Kivy sucks at dealing with parameters on the command line
    So that's an ugly wrap…
    """
    args, kivy_args = parser.parse_known_args()
    sys.argv[1:] = kivy_args
    from sadkivy.app import *
    Cartwallpy(media_dirs=args.media_dir).run()
