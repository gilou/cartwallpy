cartopy: a simple cartwall implemented in Python using Kivy
======


Cartopy is designed as a very simple cartwall: an app allowing you to play audio files by clicking on the cart they are loaded in.

For now, it finds all the .mp3 files in the dir it's started from, sort them and load each in a cart.

Usage
====

python main.py
